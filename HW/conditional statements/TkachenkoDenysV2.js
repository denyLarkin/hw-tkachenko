//Если а – четное посчитать а*б, иначе а+б
function odd(a, b) {
    if (typeof a !== "number" || typeof b !== "number") {
        return "NaN";
    }

    if (a % 2 === 0) {
        return a * b;
    }
    return a + b;
}


//Определить какой четверти принадлежит точка с координатами (х,у)
function coordinates(x, y) {
    if (typeof x !== "number" || typeof y !== "number") {
        return "NaN";
    }

    if (x === 0 && y === 0) {
        return "точка находится в начале координат";
    }

    if (x === 0) {
        if (y > 0) {
            return "Лежит на оси Y между 1 и 2 четвертями";
        }
        return "Лежит на оси Y между 3 и 4 четвертями";
    }

    if (y === 0) {
        if (x > 0) {
            return "Лежит на оси X между 1 и 4 четвертями";
        }
        return "Лежит на оси X между 2 и 3 четвертями";
    }

    if (x > 0) {
        if (y > 0) {
            return "1 Четверть";
        }
        return "4 Четверть";
    }

    if (x < 0) {
        if (y > 0) {
            return "2 Четверть";
        }
        return "3 Четверть";
    }
}


//Найти суммы только положительных из трех чисел
function positive(a, b, c) {
    if (typeof a !== "number" || typeof b !== "number" || typeof c !== "number") {
        return "NaN";
    }
    var res = 0;

    if (a > 0) {
        res += a;
    }
    if (b > 0) {
        res += b;
    }
    if (c > 0) {
        res += c;
    }

    return res;

}


//Посчитать выражение max(а*б*с, а+б+с)+3
function getMax(a, b, c) {
    if (typeof a !== "number" || typeof b !== "number" || typeof c !== "number") {
        return "NaN";
    }
    var sum = a + b + c;
    var multiply = a * b * c;

    if (sum === multiply) {
        return sum + 3;
    }
    return (sum > multiply) ? sum + 3 : multiply + 3;
}


//Программа определения оценки студента по его рейтингу, на основе правил
function raiting(a) {
    if (typeof (a) !== "number") {
        return "NaN";
    }
    if (a < 0) {
        return "Ваш рейтинг очень плохой, начните заниматься!";
    }
    if (a >= 0 && a <= 19) {
        return "У вас F";
    }
    if (a <= 39) {
        return "У вас E";
    }
    if (a <= 59) {
        return "У вас D";
    }
    if (a <= 74) {
        return "У вас C";
    }
    if (a <= 89) {
        return "У вас B";
    }
    if (a <= 100) {
        return "У вас A";
    }
    return "Вы Мошенник";
}

