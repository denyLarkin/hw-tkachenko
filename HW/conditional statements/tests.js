describe("Testing Conditional Statements", function () {

    describe("Если а – четное посчитать а*б, иначе а+б", function () {
        it("Если а-even", function () {
            assert.strictEqual(odd(2, 3), 6);
        });
        it("Если а-odd", function () {
            assert.strictEqual(odd(1, 3), 4);
        });
        it("Если а<0-even", function () {
            assert.strictEqual(odd(-2, 3), -6);
        });
        it("Если а<0-odd", function () {
            assert.strictEqual(odd(-3, 3), 0);
        });
        it("Если а-NaN", function () {
            assert.strictEqual(odd("fq", 3), "NaN");
        });
        it("Если b-NaN", function () {
            assert.strictEqual(odd(-3, "qwe"), "NaN");
        });
    });

    describe("Определить какой четверти принадлежит точка с координатами (х,у)", function () {
        it("Начало координат x=0, y=0", function () {
            assert.strictEqual(coordinates(0, 0), "точка находится в начале координат");
        });
        it("На оси x=0, y>0", function () {
            assert.strictEqual(coordinates(0, 1), "Лежит на оси Y между 1 и 2 четвертями");
        });
        it("На оси x=0, y<0", function () {
            assert.strictEqual(coordinates(0, -1), "Лежит на оси Y между 3 и 4 четвертями");
        });
        it("На оси y=0, x>0", function () {
            assert.strictEqual(coordinates(1, 0), "Лежит на оси X между 1 и 4 четвертями");
        });
        it("На оси y=0, x<0", function () {
            assert.strictEqual(coordinates(-1, 0), "Лежит на оси X между 2 и 3 четвертями");
        });
        it("1 Четверть x>0, y>0", function () {
            assert.strictEqual(coordinates(1, 1), "1 Четверть");
        });
        it("2 Четверть x<0, y>0", function () {
            assert.strictEqual(coordinates(-1, 1), "2 Четверть");
        });
        it("3 Четверть x<0, y<0", function () {
            assert.strictEqual(coordinates(-1, -1), "3 Четверть");
        });
        it("4 Четверть, y<0", function () {
            assert.strictEqual(coordinates(1, -1), "4 Четверть");
        });
        it("Некорректный ввод x=NaN", function () {
            assert.strictEqual(coordinates("fr", -1), "NaN");
        });
        it("Некоректный ввод y=NaN", function () {
            assert.strictEqual(coordinates(1, "rf"), "NaN");
        });
    });

    describe("Найти суммы только положительных из трех чисел", function () {
        it("Все нули", function () {
            assert.strictEqual(positive(0, 0, 0), 0);
        });
        it("Только a", function () {
            assert.strictEqual(positive(1, -4, -5), 1);
        });
        it("Только b", function () {
            assert.strictEqual(positive(-3, 4, -2), 4);
        });
        it("Только с", function () {
            assert.strictEqual(positive(-1, -2, 3), 3);
        });
        it("Только a, b", function () {
            assert.strictEqual(positive(1, 1, 0), 2);
        });
        it("Только a, c", function () {
            assert.strictEqual(positive(1, -1, 1), 2);
        });
        it("Только b, c", function () {
            assert.strictEqual(positive(-1, 1, 1), 2);
        });
        it("Все положительные", function () {
            assert.strictEqual(positive(1, 1, 1), 3);
        });
        it("Все отрицательные", function () {
            assert.strictEqual(positive(-1, -1, -1), 0);
        });
        it("Некорректный ввод a=NaN", function () {
            assert.strictEqual(positive("fr", 0, -1), "NaN");
        });
        it("Некоректный ввод b=NaN", function () {
            assert.strictEqual(positive(1, "f", 0), "NaN");
        });
        it("Некорректный ввод c=NaN", function () {
            assert.strictEqual(positive(1, 0, "d"), "NaN");
        });
    });

    describe("Посчитать выражение max(а*б*с, а+б+с)+3", function () {
        it("Все нули", function () {
            assert.strictEqual(getMax(0, 0, 0), 3);
        });
        it("Умножение больше a>1,b>1,c>1", function () {
            assert.strictEqual(getMax(2, 2, 2), 11);
        });
        it("Все единицы", function () {
            assert.strictEqual(getMax(1, 1, 1), 6);
        });
        it("Есть одно отрицательное число", function () {
            assert.strictEqual(getMax(1, -2, 1), 3);
        });
        it("Два отрицательных числа", function () {
            assert.strictEqual(getMax(-1, -1, 1), 4);
        });
        it("Некорректный ввод a=NaN", function () {
            assert.strictEqual(getMax("fr", 0, -1), "NaN");
        });
        it("Некоректный ввод b=NaN", function () {
            assert.strictEqual(getMax(1, "f", 0), "NaN");
        });
        it("Некорректный ввод c=NaN", function () {
            assert.strictEqual(getMax(1, 0, "d"), "NaN");
        });
    });

    describe("Написать программу определения оценки студента по его рейтингу,", function () {
        it("Рейтинг больше 100", function () {
            assert.strictEqual(raiting(101), "Вы Мошенник");
        });
        it("Рейтинг А", function () {
            assert.strictEqual(raiting(90), "У вас A");
        });
        it("Рейтинг B", function () {
            assert.strictEqual(raiting(75), "У вас B");
        });
        it("Рейтинг C", function () {
            assert.strictEqual(raiting(60), "У вас C");
        });
        it("Рейтинг D", function () {
            assert.strictEqual(raiting(40), "У вас D");
        });
        it("Рейтинг E", function () {
            assert.strictEqual(raiting(20), "У вас E");
        });
        it("Рейтинг F", function () {
            assert.strictEqual(raiting(0), "У вас F");
        });
        it("Рейтинг ниже F", function () {
            assert.strictEqual(raiting(-1), "Ваш рейтинг очень плохой, начните заниматься!");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(raiting("A"), "NaN");
        });
    });
});