//Валидатор
function validator(a) {
    if (typeof a !== "number") {
        return true;
    }
    if (a <= 0) {
        return true;
    }
    return false;

}

//Найти сумму четных чисел и их количество в диапазоне от 1 до 99
function getSumEven() {
    var resSum = 0;
    var amount = 0;
    for (var index = 1; index < 99; index++) {
        if (index % 2 === 0) {
            resSum += index;
            amount++;
        }
    }
    return "Сумма четных = " + resSum + ", их количество = " + amount;
}

//Проверить простое ли число?
function simple(a) {
    if (typeof a !== "number") {
        return "NaN";
    }

    if (a === 1 || a <= 0) {
        return "Не простое число";
    }

    var res = 0;
    for (var index = 1; index <= a; index++) {
        if (a % index === 0) {
            res++;
            if (res > 2) {
                return "Не простое число";
            }
        }
    }

    return "Простое число";
}

//Найти корень натурального числа с точностью до целого (ПОДБОР)
function getSqrt1(a) {
    if (validator(a)) {
        return "Ошибка";
    }

    for (var index = 0; index <= a + 1; index++) {
        if (index * index > a) {
            return index - 1;
        }
    }
}

//Найти корень натурального числа с точностью до целого (БИНАРКА)
function getSqrt2(a) {
    if (validator(a)) {
        return "Ошибка";
    }
    if (a === 1) {
        return 1;
    }
    var first = 0;
    var last = a;
    var middle = 0;

    while (first < last - 1) {
        middle = ((first + last) - ((first + last) % 2)) / 2;
        if (middle * middle == a) {
            return middle;
        } else if (middle * middle > a) {
            last = middle;
        } else {
            first = middle;
        }
    }
    return first;
}


//Вычислить факториал числа n.
function factorial(a) {
    if (validator(a)) {
        return "Ошибка";
    }

    var res = 1;
    for (var index = 1; index <= a; index++) {
        res *= index;
    }
    return res;
}

//Посчитать сумму цифр заданного числа
function getSumNum(a) {
    if (typeof a !== "number") {
        return "NaN";
    }
    var res = 0;
    if(a<0){
        a*=-1;
    }
    while (a > 0) {
        res = res + a % 10;
        a = (a - a % 10) / 10;
    }
    return res;
}

//Вывести число, которое является зеркальным отображением последовательности цифр заданного числа
function getMirror(a) {
    if(a===0){
        return 0;
    }
    if (validator(a)) {
        return "Ошибка";
    }
    var res = 0;
    var i = 1;
    while (a > 0) {
        res = res * i + (a % 10);
        a = (a - a % 10) / 10;
        i = 10;
    }
    return res;
}
