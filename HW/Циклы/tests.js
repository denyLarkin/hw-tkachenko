describe("Testing Cycle", function () {

    describe("Найти сумму четных чисел и их количество в диапазоне от 1 до 99", function () {
        it("Выполнение", function () {
            assert.strictEqual(getSumEven(), "Сумма четных = 2450, их количество = 49");
        });
    });

    describe("Проверить простое ли число?", function () {
        it("Проверка 0", function () {
            assert.strictEqual(simple(0), "Не простое число");
        });
        it("Проверка 1", function () {
            assert.strictEqual(simple(1), "Не простое число");
        });
        it("Проверка 2", function () {
            assert.strictEqual(simple(2), "Простое число");
        });
        it("Проверка 3", function () {
            assert.strictEqual(simple(3), "Простое число");
        });
        it("Проверка 4", function () {
            assert.strictEqual(simple(4), "Не простое число");
        });
        it("Проверка 112", function () {
            assert.strictEqual(simple(112), "Не простое число");
        });
        it("Проверка 113", function () {
            assert.strictEqual(simple(113), "Простое число");
        });
        it("Проверка отрицательного", function () {
            assert.strictEqual(simple(-4), "Не простое число");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(simple("def"), "NaN");
        });
    });

    describe("Найти корень натурального числа с точностью до целого (ПОДБОР)", function () {
        it("Проверка 0", function () {
            assert.strictEqual(getSqrt1(0), "Ошибка");
        });
        it("Проверка 1", function () {
            assert.strictEqual(getSqrt1(1), 1);
        });
        it("Проверка 2", function () {
            assert.strictEqual(getSqrt1(2), 1);
        });
        it("Проверка 4", function () {
            assert.strictEqual(getSqrt1(4), 2);
        });
        it("Проверка 16", function () {
            assert.strictEqual(getSqrt1(16), 4);
        });
        it("Проверка 112", function () {
            assert.strictEqual(getSqrt1(112), 10);
        });
        it("Проверка отрицательного", function () {
            assert.strictEqual(getSqrt1(-4), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getSqrt1("def"), "Ошибка");
        });
    });

    describe("Найти корень натурального числа с точностью до целого (БИНАРКА)", function () {
        it("Проверка 0", function () {
            assert.strictEqual(getSqrt2(0), "Ошибка");
        });
        it("Проверка 1", function () {
            assert.strictEqual(getSqrt2(1), 1);
        });
        it("Проверка 2", function () {
            assert.strictEqual(getSqrt2(2), 1);
        });
        it("Проверка 4", function () {
            assert.strictEqual(getSqrt2(4), 2);
        });
        it("Проверка 16", function () {
            assert.strictEqual(getSqrt2(16), 4);
        });
        it("Проверка 112", function () {
            assert.strictEqual(getSqrt2(112), 10);
        });
        it("Проверка отрицательного", function () {
            assert.strictEqual(getSqrt2(-4), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getSqrt2("def"), "Ошибка");
        });
    });

    describe("Вычислить факториал числа", function () {
        it("Проверка 0", function () {
            assert.strictEqual(factorial(0), "Ошибка");
        });
        it("Проверка 1", function () {
            assert.strictEqual(factorial(1), 1);
        });
        it("Проверка 2", function () {
            assert.strictEqual(factorial(2), 2);
        });
        it("Проверка 3", function () {
            assert.strictEqual(factorial(3), 6);
        });
        it("Проверка 4", function () {
            assert.strictEqual(factorial(4), 24);
        });
        it("Проверка 112", function () {
            assert.strictEqual(factorial(11), 39916800);
        });
        it("Проверка отрицательного", function () {
            assert.strictEqual(factorial(-4), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(factorial("def"), "Ошибка");
        });
    });

    describe("Посчитать сумму цифр заданного числа", function () {
        it("Проверка 0", function () {
            assert.strictEqual(getSumNum(0), 0);
        });
        it("Проверка 1", function () {
            assert.strictEqual(getSumNum(1), 1);
        });
        it("Проверка 22", function () {
            assert.strictEqual(getSumNum(22), 4);
        });
        it("Проверка 333", function () {
            assert.strictEqual(getSumNum(333), 9);
        });
        it("Проверка 112", function () {
            assert.strictEqual(getSumNum(112), 4);
        });
        it("Проверка отрицательного", function () {
            assert.strictEqual(getSumNum(-44), 8);
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getSumNum("def"), "NaN");
        });
    });

    describe("Вывести число, которое является зеркальным отображением последовательности цифр заданного числа", function () {
        it("Проверка 0", function () {
            assert.strictEqual(getMirror(0), 0);
        });
        it("Проверка 1", function () {
            assert.strictEqual(getMirror(1), 1);
        });
        it("Проверка 23", function () {
            assert.strictEqual(getMirror(23), 32);
        });
        it("Проверка 321", function () {
            assert.strictEqual(getMirror(321), 123);
        });
        it("Проверка 1112", function () {
            assert.strictEqual(getMirror(1112), 2111);
        });
        it("Проверка отрицательного", function () {
            assert.strictEqual(getMirror(-44), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getMirror("def"), "Ошибка");
        });
    }); 
});