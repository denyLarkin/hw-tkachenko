
describe("Testing Array", function () {

    describe("Найти минимальный элемент массива", function () {
        it("Выполнение", function () {
            assert.strictEqual(getMinElement([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]), -10);
        });
        it("Пустой массив", function () {
            assert.strictEqual(getMinElement([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getMinElement("da"), "Ошибка");
        });
    });

    describe("Найти максимальный элемент массива", function () {
        it("Выполнение", function () {
            assert.strictEqual(getMaxElement([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]), 7);
        });
        it("Пустой массив", function () {
            assert.strictEqual(getMaxElement([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getMaxElement("da"), "Ошибка");
        });
    });

    describe("Найти индекс минимального элемента массива", function () {
        it("Выполнение", function () {
            assert.strictEqual(getMinElementIndex([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]), 9);
        });
        it("Пустой массив", function () {
            assert.strictEqual(getMinElementIndex([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getMinElementIndex("da"), "Ошибка");
        });
    });

    describe("Найти индекс максимального элемента массива", function () {
        it("Выполнение", function () {
            assert.strictEqual(getMaxElementIndex([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]), 7);
        });
        it("Пустой массив", function () {
            assert.strictEqual(getMaxElementIndex([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getMaxElementIndex("da"), "Ошибка");
        });
    });

    describe("Посчитать сумму элементов массива с нечетными индексами", function () {
        it("Выполнение", function () {
            assert.strictEqual(getSumElementOddIndex([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]), 6);
        });
        it("Пустой массив", function () {
            assert.strictEqual(getSumElementOddIndex([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getSumElementOddIndex("da"), "Ошибка");
        });
    });

    describe("Сделать реверс массива (массив в обратном направлении)", function () {
        it("Выполнение", function () {
            assert.strictEqual(getRevaersArr([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]).toString(), "-10,-2,7,6,5,4,3,2,1,0");
        });
        it("Пустой массив", function () {
            assert.strictEqual(getSumElementOddIndex([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getSumElementOddIndex("da"), "Ошибка");
        });
    });

    describe("Посчитать количество нечетных элементов массива", function () {
        it("Выполнение", function () {
            assert.strictEqual(getAmountOddElement([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]), 4);
        });
        it("Пустой массив", function () {
            assert.strictEqual(getAmountOddElement([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getAmountOddElement("da"), "Ошибка");
        });
    });

    describe("Поменять местами первую и вторую половину массива", function () {
        it("Выполнение (четное количество элементов)", function () {
            assert.strictEqual(getSwapHalf([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]).toString(), "5,6,7,-2,-10,0,1,2,3,4");
        });
        it("Выполнение (нечетное количество элементов)", function () {
            assert.strictEqual(getSwapHalf([0, 1, 2, 3, 4, 5, 6, 7, -2]).toString(), "4,5,6,7,-2,0,1,2,3");
        });
        it("Пустой массив", function () {
            assert.strictEqual(getSwapHalf([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getSwapHalf("da"), "Ошибка");
        });
    });

    describe("Отсортировать массив пузырьком (Bubble)", function () {
        it("Выполнение", function () {
            assert.strictEqual(bubble([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]).toString(), "-10,-2,0,1,2,3,4,5,6,7");
        });
        it("Пустой массив", function () {
            assert.strictEqual(bubble([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(bubble("da"), "Ошибка");
        });
    });

    describe("Отсортировать массив выбором (Select)", function () {
        it("Выполнение", function () {
            assert.strictEqual(select([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]).toString(), "-10,-2,0,1,2,3,4,5,6,7");
        });
        it("Пустой массив", function () {
            assert.strictEqual(select([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(select("da"), "Ошибка");
        });
    });

    describe("Отсортировать массив вставками (Insert)", function () {
        it("Выполнение", function () {
            assert.strictEqual(insert([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]).toString(), "-10,-2,0,1,2,3,4,5,6,7");
        });
        it("Пустой массив", function () {
            assert.strictEqual(insert([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(insert("da"), "Ошибка");
        });
    });

    describe("Отсортировать массив слиянием (merge)", function () {
        it("Выполнение", function () {
            assert.strictEqual(mergeDiv([0, 1, 2, 3, 4, 5, 6, 7, -2, -10]).toString(), "-10,-2,0,1,2,3,4,5,6,7");
        });
        it("Выполнение", function () {
            assert.strictEqual(mergeDiv([8,7,6,5,4,3,2,1]).toString(), "1,2,3,4,5,6,7,8");
        });
        it("Пустой массив", function () {
            assert.strictEqual(mergeDiv([]), "Ошибка");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(mergeDiv("da"), "Ошибка");
        });
    });

});