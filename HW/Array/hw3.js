function validator(array) {
    if (Array.isArray(array)) {
        if (array.length == 0) {
            return true;
        }
        return false;
    }
    return true;
}

//Найти минимальный элемент массива
function getMinElement(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let min = array[0];
    for (let index = 1; index < array.length; index++) {
        if (min > array[index]) {
            min = array[index];
        }
    }
    return min;
}

//Найти максимальный элемент массива
function getMaxElement(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let max = array[0];
    for (let index = 1; index < array.length; index++) {
        if (max < array[index]) {
            max = array[index];
        }
    }
    return max;
}

//Найти индекс минимального элемента массива
function getMinElementIndex(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let minIndex = 0;
    for (let index = 1; index < array.length; index++) {
        if (array[minIndex] > array[index]) {
            minIndex = index;
        }
    }
    return minIndex;
}

//Найти индекс максимального элемента массива
function getMaxElementIndex(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let max = array[0];
    let k;
    for (let index = 1; index < array.length; index++) {
        if (max < array[index]) {
            max = array[index];
            k = index;
        }
    }
    return k;
}

//Посчитать сумму элементов массива с нечетными индексами
function getSumElementOddIndex(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let sum = 0;
    for (let index = 1; index < array.length; index += 2) {
        sum += array[index];
    }
    return sum;
}

//Сделать реверс массива (массив в обратном направлении)
function getRevaersArr(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let newArr = [];
    k = 0;
    for (let index = array.length - 1; index >= 0; index--) {
        newArr[k] = array[index];
        k++;
    }
    return newArr;
}

//Посчитать количество нечетных элементов массива
function getAmountOddElement(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let amount = 0;
    for (let index = 0; index < array.length; index++) {
        if (array[index] % 2 !== 0) {
            amount++;
        }
    }
    return amount;
}

//Поменять местами первую и вторую половину массива, например, для массива 1 2 3 4, результат 3 4 1 2
function getSwapHalf(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    let half = (array.length - ((array.length) % 2)) / 2;
    let k = 0;
    let newArr = [];
    for (let index = half; index < array.length; index++) {
        newArr[k] = array[index];
        k++;
    }
    for (let index = 0; index < half; index++) {
        newArr[k] = array[index];
        k++;
    }
    return newArr;
}

//Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
function bubble(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    for (let index = 0; index < array.length; index++) {
        for (let k = 0; k < array.length; k++) {
            if (array[k] > array[k + 1]) {
                let temp = array[k];
                array[k] = array[k + 1];
                array[k + 1] = temp;
            }
        }
    }
    return array;
}

function select(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    for (let index = 0; index < array.length; index++) {
        let min = index;
        for (let k = index + 1; k < array.length; k++) {
            if (array[min] > array[k]) {
                min = k;
            }
        }
        if (min !== index) {
            let temp = array[index];
            array[index] = array[min];
            array[min] = temp;
        }
    }
    return array;
}

function insert(array) {
    if (validator(array)) {
        return "Ошибка";
    }
    for (let index = 1; index < array.length; index++) {
        var current = array[index];
        let k = index;
        while (k > 0 && array[k - 1] > current) {
            array[k] = array[k - 1];
            k--;
        }
        array[k] = current;
    }
    return array;
}





function mergeDiv(array) {
    if (validator(array) === true) {
        return "Ошибка";
    }

    if (array.length <= 1) {
        return array;
    }

    var middle = (array.length - ((array.length) % 2)) / 2;
    if (array.length % 2 !== 0) {
        middle++;
    }

    var left = getLeft();
    function getLeft() {
        var newArr = [];
        for (var i = 0; i < middle; i++) {
            newArr[i] = array[i];
        }
        return newArr;
    }

    var right = getRight();
    function getRight() {
        var newMiddle = middle;
        var newArr = [];
        var rightLength = array.length - newMiddle - 1;
        for (let i = 0; i <= rightLength; i++) {
            newArr[i] = array[newMiddle];
            newMiddle++;
        }
        return newArr;
    }
    return merge(mergeDiv(left), mergeDiv(right));
}

function merge(left, right) {
    var res = [];
    var leftIndex = 0;
    var rightIndex = 0;
    while (left.length > leftIndex && right.length > rightIndex) {
        if (left[leftIndex] < right[rightIndex]) {
            res[res.length] = left[leftIndex];
            leftIndex++;
        }
        else {
            res[res.length] = right[rightIndex];
            rightIndex++;
        }
    }

    var leftLength = left.length - leftIndex;
    for (var i = 0; i < leftLength; i++) {
        res[res.length] = left[i + leftIndex];

    }
    var rightLength = right.length - rightIndex;
    for (var i = 0; i < rightLength; i++) {
        res[res.length] = right[i + rightIndex];

    }

    return res;
}
