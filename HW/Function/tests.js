

describe("Testing Function", function () {

    describe("Получить строковое название дня недели по номеру дня", function () {
        it("Выполнение Пн", function () {
            assert.strictEqual(getDay(1), "Понедельник");
        });
        it("Выполнение Вс", function () {
            assert.strictEqual(getDay(7), "Воскресенье");
        });
        it("Ввели 8", function () {
            assert.strictEqual(getDay(8), "Введите число от 1 до 7");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getDay("da"), "Введите число от 1 до 7");
        });
    });

    describe("Найти расстояние между двумя точками в двумерном декартовом пространстве", function () {
        it("Выполнение", function () {
            assert.strictEqual(getDistance(1,2,3,-6), 8);
        });
        it("Точки совпадают", function () {
            assert.strictEqual(getDistance(1,1,1,1), 0);
        });
        it("Точки лежат на одной прямой", function () {
            assert.strictEqual(getDistance(1,2,1,8), 6);
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getDistance("da"), "Ошибка");
        });
    });

    describe("Вводим число (0-999), получаем строку с прописью числа.", function () {
        it("Выполнение (555)", function () {
            assert.strictEqual(getName(555), "пятьсот пятьдесят пять");
        });
        it("Ноль", function () {
            assert.strictEqual(getName(0), "ноль");
        });
        it("Выполнение (12)", function () {
            assert.strictEqual(getName(12), "двенадцать ");
        });
        it("Выполнение (112)", function () {
            assert.strictEqual(getName(112), "сто двенадцать ");
        });
        it("Выполнение (1)", function () {
            assert.strictEqual(getName(1), "один");
        });
        it("Выполнение (25)", function () {
            assert.strictEqual(getName(25), "двадцать пять");
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getName("da"), "Введите число от 0-999");
        });
    });

    describe("Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число.", function () {
        it("Выполнение (555)", function () {
            assert.strictEqual(getNumber("пятьсот пятьдесят пять"), 555);
        });
        it("Ноль", function () {
            assert.strictEqual(getNumber("ноль"), 0);
        });
        it("Выполнение (25)", function () {
            assert.strictEqual(getNumber("двадцать пять"), 25);
        });
        it("Выполнение (11)", function () {
            assert.strictEqual(getNumber("одиннадцать"), 11);
        });
        it("Выполнение (112)", function () {
            assert.strictEqual(getNumber("сто двенадцать"), 112);
        });
        it("Некорректный ввод", function () {
            assert.strictEqual(getNumber("da"), "Ошибка");
        });
    });

});