
var first = true;
var arr = [];
var result = null;
var min = null;
var max = null;

function getValue() {//Получаем значения
    if (first) {
        min = document.getElementById("min");
        max = document.getElementById("max");
        if (validator(parseInt(min.value), parseInt(max.value))) {
            return alert("Введите положительное число");
        }
        min.disabled = true;
        document.getElementById("max").disabled = true;
        document.getElementById("repeatOn").disabled = true;
        first = false;
    }
    generator(min.value);
}

function generator() {//Генерирует и выводит

    if (arr.length === max - min + 1) { //Валидация: Остались ли числа?
        document.getElementById("generateButton").disabled = true;
        return alert("Числа закончились, нажмите Reset");
    }

    do {
        result = min + Math.random() * (max - min + 1); //Достаем число
        result = result - result % 1;
        //newNum = checkOverlap();
    } while (newNum() === false)
    document.getElementById("output").innerHTML = result;
}

function reset() {//Сбрасывает
    arr = [];
    document.getElementById("output").innerHTML = '';
    document.getElementById("min").value = '';
    document.getElementById("max").value = '';
    document.getElementById("min").disabled = false;
    document.getElementById("max").disabled = false;
    document.getElementById("repeatOn").checked = false;
    document.getElementById("repeatOn").disabled = false;
    document.getElementById("generateButton").disabled = false;
    first = true;
}

function newNum() { //повторяетются ли числа?
    if(document.getElementById("repeatOn").checked===true){
        return true;
    }
    for (let index = 0; index < arr.length; index++) {
        if (result === arr[index]) {
            return false;
        }
    }
    arr[arr.length] = result;
    return true;
}

function validator(minValue, maxValue) {//Валидатор
    if (isNaN(minValue) || isNaN(maxValue)) {
        return true;
    }
    if (minValue > maxValue) {
        var temp = maxValue;
        maxValue = minValue;
        minValue = temp;
        document.getElementById("min").value = minValue;
        document.getElementById("max").value = maxValue;
    }
    if (minValue < 0) {
        return true;
    }

    return false;

}
