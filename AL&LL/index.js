var IList = function () {};

// IList.prototype.init = () => {}; // метод для инициализации var array = new ArrayList(); array.init([1,2,3,4,5])
// IList.prototype.toString = () => {}; // +
// IList.prototype.getSize = () => {}; // +
// IList.prototype.push = (value) => {}; // +
// IList.prototype.pop = () => {};
// IList.prototype.shift = () => {};
// IList.prototype.unshift = (value) => {};
// IList.prototype.slice = (start, end) => {};
// IList.prototype.splice = (start, numberToSplice, ...elements) => {};
// IList.prototype.sort = (comparator) => {}; // comparator ==> callback
// IList.prototype.get = (index) => {}; // get by index
// IList.prototype.set = (index, element) => {}; // set element by index

//ArrayList
var ArrayList = function() {
    IList.apply(this, arguments);
    this.array = [];
};
// ArrayList.prototype = Object.create(IList.prototype);
ArrayList.prototype.constructor = ArrayList;

ArrayList.prototype.push = function(value) {
    this.array[this.array.length] = value;
};
ArrayList.prototype.getSize = function(value) {
    return this.array.length;
};

ArrayList.prototype.init = function(initialArray) {
    for (var i = 0; i < initialArray.length; i++) {
        this.push(initialArray[i]);
    }
    console.log('initialArray', this.array);
    return this.array;
};

ArrayList.prototype.toString = function() {
    let arrayString = "";
    for(let i = 0; i < this.array.length; i++){
        arrayString += this.array[i];
    }
    console.log("ArrayToString", arrayString);
    return arrayString;
};

ArrayList.prototype.pop = function(initialArray){
    let lastElement = initialArray[initialArray.length - 1];
    console.log("PopLastElement", lastElement);
    for(let i = 0; i < initialArray.length - 1; i++){
        this.push(initialArray[i]);
    }
    console.log("PopArray", this.array);
};

ArrayList.prototype.shift = function(initialArray){
    let firstElement = initialArray[0];
    console.log("shiftFirstElement", firstElement);
    for(let i = 1; i < initialArray.length; i++){
        this.push(initialArray[i]);
    }
    console.log("shiftArray", this.array);
};

ArrayList.prototype.unshift = function(){
    let array1 = [];
    for(let i = 0; i < arguments.length; i++){
            array1[i] = arguments[i];
    }
    // for(let m = 0; m < this.array.length; m++) {
        for (let h = array1.length, m = 0; h < (this.array.length + array1.length), m < this.array.length; h++, m++) {
            array1[h] = this.array[m];
        }
    // }
    this.array = array1;
    console.log("shiftArray", this.array);
};

ArrayList.prototype.splice = function(start, deleteCounter) {
    let tmpArray = new ArrayList();

    let removedArray = [];

    if (start > 0) {
        for (let i = 0; i < start; i++) {
            tmpArray[tmpArray.length] = this.array[i];
        }
    }

    for (let i = start; i < deleteCounter + start; i++) {
        removedArray[removedArray.length] = this.array[i];
    }

    if(arguments.length > 0) {
        for (let i = 2; i < arguments.length; i++) {
            tmpArray[tmpArray.length] = arguments[i];
        }
    }

    for (let i = deleteCounter + start; i < this.array.length; i++) {
        tmpArray[tmpArray.length] = this.array[i];
    }
    console.log(tmpArray);
    this.array = tmpArray;
    return this.array;
};

ArrayList.prototype.splice = function(){
    let array1 = [];
    let countOfArg = arguments.length;
    let count = 0;
        if(arguments[0] >= 0){
            for(let i = 0; i < this.array.length ;i++){
                if(i < arguments[0] || i > arguments[1]){
                    array1[count] = this.array[i];
                    count++;
                }else if(countOfArg > 2){
                    for(let j = 2; j < arguments.length; j++) {
                        array1[count] = arguments[j];
                        count++;
                    }
                    countOfArg = 0;
                }
            }
        }
    this.array = array1;
    console.log("spliceArray", this.array);
};

ArrayList.prototype.sort = function() {
    for (let i = 0; i < this.array.length - 1; i++) {
        for (let j = 0; j < this.array.length - 1 - i; j++) {
            if (this.array[j] > this.array[j+1]) {
                let temp = this.array[j+1];
                this.array[j+1] = this.array[j];
                this.array[j] = temp
            }
        }
    }
    console.log("sortArray", this.array);
    return this.array
};

ArrayList.prototype.get = function(index) {
    console.log(this.array[index]);
    return this.array[index];
};

ArrayList.prototype.set = function(index, element) {
    this.array[index] = element;
    console.log(this.array);
    return this.array;
};


let arr = new ArrayList();
arr.init([1, 2, 3, 4, 5]);
arr.toString([1, 2, 3]);
// arr.pop([1, 2, 3]);
// arr.shift([1, 2, 3]);
// arr.unshift("Привет", "пока");
// arr.slice(-2);
// arr.splice(1, 2, "Ку", "hbkh", "hjkjnhgthj", 123);
// arr.sort();
// arr.get(2);
// arr.set(2, "hjkl");
console.log(arr);


// LinkedList
let Node = function(value) {
    this.value = value;
    this.next = null;
};
let LinkedList = function() {
    IList.apply(this, arguments);
    this.root = null;
};
LinkedList.prototype = Object.create(IList.prototype);
LinkedList.prototype.constructor = LinkedList;

LinkedList.prototype.getSize = function() {
    let tempNode = this.root;
    let size = 0;
    while(tempNode !== null) {
        tempNode = tempNode.next;
        size++;
    }
    return size;
};

LinkedList.prototype.unshift = function(value) {
    let size = this.getSize();
    let node = new Node(value);
    node.next = this.root;
    this.root = node;
    return size + 1;
};

LinkedList.prototype.toString = function(){
    let str = '';
    let tempNode = this.root;
    while(tempNode !== null){
        str += tempNode.value;
        tempNode = tempNode.next;
    }
    console.log(str);
    return str;
};


LinkedList.prototype.init = function(initialArray) {
    for (let i = initialArray.length -1 ; i >=0; i--) {
        this.unshift(initialArray[i]);
    }
};

LinkedList.prototype.push = function(value){
    let size = this.getSize();
    let node = new Node(value);
    let tempNode = this.root;
    while(tempNode.next !== null){
        tempNode = tempNode.next;
    }
    tempNode.next = node;
    size++;
    return size;
};

LinkedList.prototype.pop = function(){
    let firstElement;
    let size = this.getSize();
    let tempNode = this.root;
    while(tempNode.next !== null){
        firstElement = tempNode;
        tempNode = tempNode.next;
    }
    firstElement.next = null;
    tempNode = null;
    size--;
    return size;
};

LinkedList.prototype.shift = function(){
    let size = this.getSize();
    let tempNode = this.root;
    this.root = tempNode.next;
    tempNode = null;
    size--;
    return size;
};

LinkedList.prototype.slice = function(start, end){
    let newArray = [];
    let count_of_operations = 0;
    let count_of_element = 0;
    let tempNode = this.root;

    while(tempNode.next !== null){
        tempNode = tempNode.next;
        count_of_operations++;
        if(count_of_operations >= start){
            count_of_element++;
            if(count_of_element < end){
                newArray[newArray.length] = tempNode.value;
            }
        }
    }
    return newArray;
};

LinkedList.prototype.get = function(index){
    let count = 0;
    let result;
    let tempNode = this.root;
    while(tempNode.next !== null){
        count++;
        tempNode = tempNode.next;
        if(count === index){
            result = tempNode;
        }
    }
    return result;
};

LinkedList.prototype.set = function(index, element){
    let size = this.getSize();
    let count = 0;
    let tempNode = this.root;
    let node = new Node(element);
    while(tempNode.next != null){
        count++;
        if(count + 1 === index){
            node.next = tempNode.next;
            tempNode.next = node;
        }else {
            tempNode = tempNode.next;
        }
    }
    return size + 1;
};

let list = new LinkedList();

list.init([1, 2, 3, 4]);
// list.getSize();
// list.unshift(9);
// list.toString();
// list.push(9);
// list.pop();
// list.shift();
// list.slice(1, 3);
// list.get(2);
// list.set(2 , 'ghbdtn');
console.log(list);
