import '../style/style.less';
let result = document.querySelector(".wrapper__result");
let numbersBtn = document.querySelectorAll(".numbers__button");
let operationsBtn = document.querySelectorAll(".operations__button");
let backspaceBtn = document.querySelector(".numbers__back");
let resetBtn = document.querySelector(".numbers__resert");
let arrOfOperations = [];
let resultNumber = null;
let temp = null;
const xhr = new XMLHttpRequest();

numbersBtn.forEach((index) => {
    index.addEventListener('click', function(){
        if(temp !== null){
            result.innerHTML = '';
            temp = null;
        }
        if(result.innerHTML === String(0)){
            result.innerHTML = ''
        }
        result.innerHTML += index.innerHTML;
    });
});

operationsBtn.forEach((index) => {
    index.addEventListener('click', function(){
        arrOfOperations.push(index.innerHTML);
        if(arrOfOperations.length > 1 && arrOfOperations[0] === '='){
            arrOfOperations.shift();
        }else if(arrOfOperations.length > 1) {
            total();
            arrOfOperations.shift();
        }else{
            resultNumber = parseInt(result.innerHTML);
            result.innerHTML = '';
        }
    });
});

backspaceBtn.addEventListener('click', function(){
    let temp = result.innerHTML;
    result.innerHTML = temp.substring(0, temp.length - 1)

});

resetBtn.addEventListener('click', function(){
    arrOfOperations.length = 0;
    resultNumber = null;
    result.innerHTML = '';
});

function total(){
        let requestBody = {
            firstNumber: resultNumber,
            secondNumber: parseInt(result.innerHTML),
            sign: arrOfOperations[0]
        };
        xhr.open("POST", 'http://localhost:3001/get_result');
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                // console.log(xhr.responseText);
                resultNumber = JSON.parse(xhr.responseText);
                result.innerHTML = resultNumber
            }
        };
        let data = JSON.stringify({requestBody});
        xhr.send(data);
        temp = arrOfOperations[0];
}