const express = require('express');
const app = express();
const port = 3001;
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next()
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`Server is listening on ${port}`)
});

app.post('/get_result', (request, response) => {
    let result;
    switch(request.body.requestBody.sign){
        case '+':
            result = request.body.requestBody.firstNumber + request.body.requestBody.secondNumber;
            break;
        case '-':
            result = request.body.requestBody.firstNumber - request.body.requestBody.secondNumber;
            break;
        case '*':
            result = request.body.requestBody.firstNumber * request.body.requestBody.secondNumber;
            break;
        case '/':
            result = request.body.requestBody.firstNumber / request.body.requestBody.secondNumber;
            break;
        default:
            result = '';
    }
    response.end(JSON.stringify(result))
});
