import { getSettings, changeLanguage, languageBox } from './helpers/settings.js';
import { loadSessionStorage, saveSessionsStorage } from './helpers/saveSession.js';
import { getElement } from './helpers/loadElements.js'

let selectBox = {
    meter: 1,
    verst: 1067,
    mile: 1609,
    foot: 0.3047851264858275,
    yard: 0.9144,
};

let onSetting = false;
let settingsButton = document.getElementById("settingsButton");
settingsButton.addEventListener("click", showSettings);
let button = document.getElementById("convertButton");
button.addEventListener("click", generate);
let closer= document.getElementById("modalCloser");
closer.addEventListener("click", showSettings);
let element = getElement();
element.input.oninput= validator;
main();
if(element.input.value==""){
    button.disabled=true;
}else {
    button.disabled=false;
};


function validator(){

    let mess=document.getElementById("message");
    let temp=element.input.value;
    let length = temp.length;
    let num= isFinite(temp);
    let language= element.selectLanguage.value;
    if(!num){
        mess.innerHTML=languageBox[language].message.positive;
        element.input.value="";
    } else
//     if(temp.includes("+")){
//         mess.innerHTML=languageBox[language].message.plus;
//     element.input.value="";
// } else
//     if(temp.includes("-")){
//         mess.innerHTML=languageBox[language].message.minus;
//         element.input.value="";
//     }else
if(num < 0){
    mess.innerHTML=languageBox[language].message.positive;
    element.input.value="";
}else
    if(length>13){
        mess.innerHTML=languageBox[language].message.short;
        element.input.value=temp.slice(0, -1);
    }else{        mess.innerHTML="";
}
    if(element.input.value==""){
        button.disabled=true;
    }else {
        button.disabled=false;
    };

}

function main() {
    if (sessionStorage.length != 0) {
        loadSessionStorage();
    }
    changeLanguage();
};


function generate() {
    let result = calculate(element);
    showResult(result, element.output);
    saveSessionsStorage(element);

};

function calculate(element) {
    let convertToMeter = () => element.input.value * selectBox[element.selectFrom.value];
    let convertFromMeter = (value) => value / selectBox[element.selectTo.value];
    return convertFromMeter(convertToMeter());
};

function showResult(result, output) {
    output.value = result;
};

function showSettings() {

    if (onSetting == false) {
        onSetting = true;
        element.modal.style.display = "block";
        getSettings();
        element.wrapper.style.filter="blur(10px)";
    } else {
        saveSessionsStorage(element);
        validator();
        modal.style.display = "none";
        element.wrapper.style.filter="none";
        onSetting = false;
    }
};

