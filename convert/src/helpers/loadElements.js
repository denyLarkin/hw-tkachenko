export {getElement};
function getElement() {
    let input = document.getElementById("input");
    let selectFrom = document.getElementById("selectElementFrom");
    let selectTo = document.getElementById("selectElementTo");
    let output = document.getElementById("output");
    let selectLanguage = document.getElementById("selectElementLanguage");
    let modal= document.getElementById("modal");
    let wrapper=document.getElementById("wrapper");
    return { input, selectFrom, selectTo, output, selectLanguage, modal, wrapper};
};
