import {saveSessionsStorage} from "./saveSession";

let languageBox = {
    rus: {
        select: {
            meter: 'Метр',
            verst: 'Верста',
            mile: 'Миля',
            foot: 'Фут',
            yard: 'Ярд',
        },
        text: {
            titleLabel: "Конвертер",
            headerLabel: "Конвертер",
            printLabel: "Введите значение",
            convertButton: "Конвертировать",
            resultLabel: "Результат",
            settingsLabel:"Настройки",
        },
        message: {
            positive:"Введите положительное чиcло",
            plus:"Не нужно писать +",
            minus:"Невозможно посчитать отрицательную длину",
            short:"Число должно быть короче!",
        }
    },
    ar: {
        select: {
            meter: 'متر',
            verst: 'الفرست',
            mile: 'ميل',
            foot: 'قدم',
            yard: 'ساحة',
        },
        text: {
            titleLabel: "محول",
            headerLabel: "محول",
            printLabel: "أدخل القيمة",
            convertButton: "تحويل",
            resultLabel: "يؤدي",
            settingsLabel:"إعدادات",
        },
        message: {
            positive:"أدخل رقم موجب",
            plus:"لا حاجة لكتابة +",
            minus:"غير قادر على حساب الطول السلبي",
            short:"يجب أن يكون الرقم أقصر!",
        }
    },
    eng: {
        select: {
            meter: 'Meter',
            verst: 'Verst',
            mile: 'Mile',
            foot: 'Foot',
            yard: 'Yard',
        },
        text: {
            titleLabel: "Converter",
            headerLabel: "Converter",
            printLabel: "Print",
            convertButton: "Convert",
            resultLabel: "Result",
            settingsLabel:"Settings",
        },
        message: {
            positive:"Enter a positive number",
            plus:"No need to write +",
            minus:"Unable to count negative length",
            short:"The number should be shorter!",
        }
    }
}
export {getSettings, changeLanguage, languageBox};
function getSettings() {
    let selectElementLanguage = document.getElementById("selectElementLanguage");
    selectElementLanguage.addEventListener("click", changeLanguage);
};

function changeLanguage() {
    let language = selectElementLanguage.value;
    let wrapper=document.getElementById("wrapper");
    let modal= document.getElementById("modal");
    if(language==="ar"){
        wrapper.setAttribute("dir","rtl");
        modal.setAttribute("dir","rtl");
    }else{
        wrapper.setAttribute("dir","ltl");
        modal.setAttribute("dir","ltl");
    }
    changeSelect(languageBox[language].select, "selectElementFrom");
    changeSelect(languageBox[language].select, "selectElementTo");
    changeText(languageBox[language].text);
};

function changeSelect(obj, selector) {
    let index = 0;
    let elementSelect = document.getElementById(selector)
    for (const key in obj) {
        elementSelect.options[index].innerHTML = obj[key];
        index++;
    }
};

function changeText(obj) {
    for (const key in obj) {
        document.getElementById(key).innerHTML = obj[key];
    }
};

