import {getElement} from './loadElements';
export {loadSessionStorage, saveSessionsStorage};
function saveSessionsStorage(element) {
    sessionStorage.setItem('input', element.input.value);
    sessionStorage.setItem('output', element.output.value);
    sessionStorage.setItem('selectFrom', element.selectFrom.value);
    sessionStorage.setItem('selectTo', element.selectTo.value);
    sessionStorage.setItem('selectLanguage', element.selectLanguage.value);

}

function loadSessionStorage() {
    let element = getElement();
    element.input.value = sessionStorage.getItem('input');
    element.output.value = sessionStorage.getItem('output');
    element.selectFrom.value = sessionStorage.getItem('selectFrom');
    element.selectTo.value = sessionStorage.getItem('selectTo');
    element.selectLanguage.value = sessionStorage.getItem('selectLanguage');
}



